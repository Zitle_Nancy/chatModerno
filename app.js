let persona = {
	nombre:'nancy',
	edad:23,
	saludar(){
		return `hola ${this.nombre} y tengo ${this.edad} años`;
	}
}

console.log(persona.saludar());