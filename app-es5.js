'use strict';

var persona = {
	nombre: 'nancy',
	edad: 23,
	saludar: function saludar() {
		return 'hola ' + this.nombre + ' y tengo ' + this.edad + ' a\xF1os';
	}
};

console.log(persona.saludar());
